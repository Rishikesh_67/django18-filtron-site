from django.conf.urls import include, url
from django.contrib import admin

urlpatterns=[
 		url(r'^register/$', 'PyApp.views.registration', name='registration'),	
        url(r"^image-posts/$",'PyApp.views.image_posts',name='image_posts'),
        url(r"^auth-users/$","PyApp.views.auth_users",name="auth_users"),
        url(r"^order-by-firstname/$","PyApp.views.order_by_firstname",name="order_by_firstname"),
        url(r"^image-posts/new/$","PyApp.views.post_a_new_image",name='post_a_new_image'),
        url(r"^success/$","PyApp.views.success",name='success'),
        url(r"^error/$","PyApp.views.error",name='error'),
        url(r"^login/$","PyApp.views.login",name='login'),
        url(r"^logout/$","PyApp.views.logout",name='logout'),
        url(r"^chartjs/line-charts/$","PyApp.views.line_charts",name="line_charts"),
        url(r"^chartjs/bar-charts/$","PyApp.views.bar_charts",name="bar_charts"),
   		url(r"^chartjs/polar-area-charts/$","PyApp.views.polar_area_charts",name="polar_area_charts"),
        url(r"^well-docs/$","PyApp.views.well_docs", name="well_docs"),
        url(r"^well-docs/create/$","PyApp.views.well_docs_create", name="well_docs_create"),
        url(r"^well-docs/full-view/(?P<id>\d+)$","PyApp.views.well_docs_single_view", name="well_docs_single_view"),
    ] 
