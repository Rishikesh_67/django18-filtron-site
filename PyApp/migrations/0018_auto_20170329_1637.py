# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('PyApp', '0017_docpost_search_keywords'),
    ]

    operations = [
        migrations.AlterField(
            model_name='docpost',
            name='search_keywords',
            field=models.CharField(max_length=50),
        ),
    ]
