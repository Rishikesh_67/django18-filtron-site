# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('PyApp', '0014_auto_20170329_1405'),
    ]

    operations = [
        migrations.AlterField(
            model_name='docpost',
            name='description',
            field=models.TextField(default=b''),
        ),
    ]
