# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('PyApp', '0015_auto_20170329_1431'),
    ]

    operations = [
        migrations.AlterField(
            model_name='docpost',
            name='posted_by',
            field=models.CharField(max_length=50),
        ),
    ]
