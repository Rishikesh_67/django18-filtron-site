# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('PyApp', '0018_auto_20170329_1637'),
    ]

    operations = [
        migrations.AlterField(
            model_name='docpost',
            name='posted_by',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='docpost',
            name='search_keywords',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='docpost',
            name='title',
            field=models.CharField(max_length=200),
        ),
    ]
