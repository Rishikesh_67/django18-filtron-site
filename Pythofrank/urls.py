# from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

#API IMPORTS
from django.conf.urls import include, url
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets
from PyAppApi import views 


#API CODE STARTS
# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff')

# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

# Routers provide an easy way of automatically determining the URL conf.
# router = routers.DefaultRouter()
# router.register(r'users', UserViewSet)

#===============================Quickstart==================
router = routers.DefaultRouter()
router.register("auth-users",views.AuthUserViewSet)

router2 = routers.DefaultRouter()
router2.register("mathematics",views.MathStudentViewSet)

router3 = routers.DefaultRouter()
router3.register("lora-evk-sensors",views.LoraEvkSensorViewSet)

#API CODE ENDS

urlpatterns = [
    url(r'^$', 'PyApp.views.home', name='home'),
    url(r"^pyapp/",include("PyApp.urls")),
    url(r'^admin/', include(admin.site.urls)),
    # url(r'^blog/', include('blog.urls')),

    #Api endpoints
    url(r'^api-view/', include(router3.urls)),
    url(r'^api/', include(router.urls)),
     url(r'^api/student/', include(router2.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r"^api/angularjs/users/$","PyAppApi.views.angular_api_users",name="angular_api_users"),
    url(r"^math-students/$","PyAppApi.views.math_score_board",name="math_score_board"),
    url(r"^sensor-desc/$","PyAppApi.views.sensor_desc",name="sensor_desc"),

    #Class based views and 1 func based view for home i.e. /view/
    url(r"^view/lora-evk-sensors/update/(?P<pk>[0-9]+)/$",views.LoraEvkSensorUpdate.as_view(),name="update"),
    url(r"^view/lora-evk-sensors/delete/(?P<pk>[0-9]+)/$",views.LoraEvkSensorDelete.as_view(),name="delete"),
    url(r"^view/lora-evk-sensors/create/new/$",views.LoraEvkSensorCreate.as_view(),name="create"),
    #lora-evk-sensors/create/  ==> 404, I need to check
    url(r"^view/$",views.home,name="home"), 
    url(r"^view/chart-view/$",views.chart_view_of_sensor_data,name="testing"),
    url(r"^view/chart-view/map/(?P<id>[0-9]+)/$",views.testing_with_map,name="testing_with_map"),
    url(r"^view/chart-view/map/single/(?P<id>[0-9]+)/$",views.testing_with_map_single,name="testing_with_map_single"),
]

if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL,document_root=settings.STATIC_ROOT)
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)