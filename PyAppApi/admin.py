from django.contrib import admin

# Register your models here.
from .models import AuthUser,MathStudent,LoraEvkSensor

admin.site.register(AuthUser)

admin.site.register(MathStudent)

admin.site.register(LoraEvkSensor)