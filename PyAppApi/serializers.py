from django.contrib.auth.models import User, Group
from .models import AuthUser,MathStudent,LoraEvkSensor

from rest_framework import serializers

class AuthUserSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = AuthUser
		fields = ("id","firstname","lastname","age","country")
		
class MathStudentSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = MathStudent
		fields = ("firstname","lastname","profile_pic","mathematics","physics","chemistry","hindi","english","chemistry","environment")
		
class LoraEvkSensorSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = LoraEvkSensor
		fields = "__all__"