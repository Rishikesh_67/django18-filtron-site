from django.shortcuts import render #Not the part of API
from django.contrib.auth.models import User,Group 
from .models import AuthUser,MathStudent,LoraEvkSensor
from .serializers import AuthUserSerializer,MathStudentSerializer,LoraEvkSensorSerializer
from rest_framework import viewsets
from django.views.generic.edit import UpdateView,DeleteView,CreateView

class AuthUserViewSet(viewsets.ModelViewSet):
	"""
		API endpoint that allows the users to be viewed or edited
	"""
	queryset = AuthUser.objects.all()
	serializer_class = AuthUserSerializer

def angular_api_users(request):
	return render(request,"angular_api_users.html",{})

#Math students

class MathStudentViewSet(viewsets.ModelViewSet):
	"""
		API endpoint that allows the users to be viewed or edited
	"""
	queryset = MathStudent.objects.all()
	serializer_class = MathStudentSerializer

def math_score_board(request):
	math_students = MathStudent.objects.all()
	return render(request,"math_score_board.html",{"math_students":math_students})

def sensor_desc(request):
	return render(request,"sensor_desc.html",{})

#...........Class based View Realted(1 function based view is for home i.e. /view/).............
class LoraEvkSensorViewSet(viewsets.ModelViewSet):
	queryset = LoraEvkSensor.objects.all()
	serializer_class = LoraEvkSensorSerializer

def home(request):
	obj = LoraEvkSensor.objects.all()
	return render(request,"home2.html",{"obj":obj})

class LoraEvkSensorUpdate(UpdateView):
	model = LoraEvkSensor
	fields = "__all__" #["device_id"]
	template_name = "lora_evk_sensor_update.html"

class LoraEvkSensorDelete(DeleteView):
	model = LoraEvkSensor
	success_url = "/view/"
	template_name = "lora_evk_sensor_delete.html"	

class LoraEvkSensorCreate(CreateView):
	model = LoraEvkSensor
	fields = "__all__"
	template_name = "lora_evk_sensor_create.html"


#.....
def chart_view_of_sensor_data(request):
	return render(request,"ang_req.html",{})

def testing_with_map(request,id):
	# print LoraEvkSensor.objects.all()
	for obj in  LoraEvkSensor.objects.all():
		print obj.id,obj.device_loc
	error = ""
	obj = LoraEvkSensor()	#Otherwise undefined obj
	print type(obj)
	try:
		print type(id)
		obj = LoraEvkSensor.objects.get(id=int(str(id)))
		print type(obj),obj
	except:
		print "Got an error"
		error = "Couldn't found any object with this id "+id+"<br><hr><a href='/view/'><span style='font-size:15px;'>Back to home</span></a>"
	print obj
	return render(request,"ang_req_with_map.html",{"obj":obj,"error":error})

def testing_with_map_single(request,id):
	# print LoraEvkSensor.objects.all()
	for obj in  LoraEvkSensor.objects.all():
		print obj.id,obj.device_loc
	error = ""
	obj = LoraEvkSensor()	#Otherwise undefined obj
	print type(obj)
	try:
		print type(id)
		obj = LoraEvkSensor.objects.get(id=int(str(id)))
		print type(obj),obj
	except:
		print "Got an error"
		error = "Couldn't found any object with this id "+id+"<br><hr><a href='/view/'><span style='font-size:15px;'>Back to home</span></a>"
	print obj
	return render(request,"ang_req_with_map_single.html",{"obj":obj,"error":error})
