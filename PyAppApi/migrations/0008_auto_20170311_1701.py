# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('PyAppApi', '0007_auto_20170311_1529'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mathstudent',
            name='fullname',
            field=models.CharField(max_length=100),
        ),
    ]
