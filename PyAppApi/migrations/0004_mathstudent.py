# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('PyAppApi', '0003_auto_20170311_1352'),
    ]

    operations = [
        migrations.CreateModel(
            name='MathStudent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('profile_pic', models.ImageField(default=b'https://cdn2.iconfinder.com/data/icons/mixed-rounded-flat-icon/512/note-512.png', upload_to=b'')),
                ('mathematics', models.PositiveIntegerField()),
                ('physics', models.PositiveIntegerField()),
                ('chemistry', models.PositiveIntegerField()),
                ('hindi', models.PositiveIntegerField()),
                ('english', models.PositiveIntegerField()),
                ('environment', models.PositiveIntegerField()),
                ('fullname', models.ForeignKey(to='PyAppApi.AuthUser')),
            ],
        ),
    ]
