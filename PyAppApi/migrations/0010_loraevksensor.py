# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('PyAppApi', '0009_auto_20170311_1704'),
    ]

    operations = [
        migrations.CreateModel(
            name='LoraEvkSensor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('device_id', models.CharField(default=b'70b3d58ff0031a17', help_text=b'A device id,it is set to default, you can change it if you know different device(sensor) id', max_length=20, blank=True)),
                ('device_loc', models.CharField(default=b'Googleplex', help_text=b'Name of the location/device on Google map,it is set to Googleplex as default, you can change it', max_length=100, blank=True)),
                ('door_status', models.IntegerField(help_text=b'0/1,it shows whether door is closed or opened', validators=[django.core.validators.MaxValueValidator(1), django.core.validators.MinValueValidator(0)])),
                ('no_of_times_door_opened', models.IntegerField(help_text=b'It should be a value between 0 and 127', validators=[django.core.validators.MaxValueValidator(127), django.core.validators.MinValueValidator(0)])),
                ('temperature', models.FloatField(default=0.0, help_text=b'it shows the temperature in celcius', blank=True)),
                ('switch1_status', models.IntegerField(help_text=b'0/1,t shows whether switch1 is pressed or not', validators=[django.core.validators.MaxValueValidator(1), django.core.validators.MinValueValidator(0)])),
                ('switch2_status', models.IntegerField(help_text=b'0/1,t shows whether switch2 is pressed or not', validators=[django.core.validators.MaxValueValidator(1), django.core.validators.MinValueValidator(0)])),
                ('latitude', models.FloatField(help_text=b'-90.0 - 90.0, it should be a value between -90.0 and 90.0, eg. 37.422000', validators=[django.core.validators.MaxValueValidator(90.0), django.core.validators.MinValueValidator(-90.0)])),
                ('longitude', models.FloatField(help_text=b'-180.0 - 180, it should be a value between -180.0 and 180.0, eg. -122.084025', validators=[django.core.validators.MaxValueValidator(180.0), django.core.validators.MinValueValidator(-180.0)])),
                ('relative_humidity', models.FloatField(default=0.0, help_text=b'0-100, it shows the percentage relative humidity', blank=True)),
                ('battery_level', models.FloatField(default=0.0, help_text=b'0-100, it shows the percentage of battery level', blank=True)),
                ('movement_status', models.IntegerField(help_text=b'0/1,it shows whether movement detected since last reporting', validators=[django.core.validators.MaxValueValidator(1), django.core.validators.MinValueValidator(0)])),
                ('reporting_interval', models.IntegerField(default=0, help_text=b'0-255, it shows the reporting interval in minutes', validators=[django.core.validators.MaxValueValidator(255), django.core.validators.MinValueValidator(0)])),
                ('gps_status', models.IntegerField(help_text=b'0/1,it shows whether GPS is On/Off', validators=[django.core.validators.MaxValueValidator(1), django.core.validators.MinValueValidator(0)])),
            ],
        ),
    ]
