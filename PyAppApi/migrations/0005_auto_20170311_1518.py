# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('PyAppApi', '0004_mathstudent'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mathstudent',
            name='chemistry',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='mathstudent',
            name='english',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='mathstudent',
            name='environment',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='mathstudent',
            name='hindi',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='mathstudent',
            name='mathematics',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='mathstudent',
            name='physics',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
